import { BaseConfig } from "./config";

export interface BaseViewConfig {
  component: string;
  tab?: string;
  list: BaseConfig[];
  name?: string;
}

export interface FullViewConfig {
  components: BaseViewConfig[];
}
