interface LinkParams {
  type: "custom" | "select";
  key: string;
  value: string;
  column: string[];
  subType: string;
  subKeys: string[];
}

export interface BaseConfig {
  [key: string]: any;
  key: string;
  displayName: string;
  renderType: string;
  sourceKeys?: string[];
  showKeys?: string[];
  navigate: boolean;
  path?: string;
  params?: LinkParams[];
}

export enum RenderType {
  SINGLE = "single",
  MULTI = "multi",
  LIST = "list",
}
