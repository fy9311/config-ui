import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { ConfigPage } from "./pages/config";
import { DetailPage } from "./pages/detail";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/config" children={<ConfigPage />}></Route>
        <Route path="/detail" children={<DetailPage />}></Route>
        <Redirect to="/config"></Redirect>
      </Switch>
    </Router>
  );
}

export default App;
