interface Table {
  [key: string]: any;
}
export const table: Table = {
  name: "xxxTable",
  creator: "fy",
  createdTime: "2021-10-01",
  columns: [
    {
      name: "column1",
      desc: "desc-column1",
    },
    {
      name: "column2",
      desc: "desc-column2",
    },
    {
      name: "column3",
      desc: "desc-column3",
    },
    {
      name: "column4",
      desc: "desc-column4",
    },
  ],
  platform: [
    {
      value: "pc",
    },
    {
      value: "mobile",
    },
    {
      value: "ps5",
    },
    {
      value: "xbox",
    },
  ],
};
