import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import { BaseViewConfig, FullViewConfig } from "../../../types/view";
import styles from "../index.module.less";
import { ConfigScreen } from "../../../components/config";
import { HeaderScreen } from "./Header";
import { ShowScreen } from "../../../components/show";
import { table } from "../../../mock";

export const ViewScreen = () => {
  const [config, setConfig] = useState<FullViewConfig>({ components: [] });
  const [visible, setVisible] = useState(false);
  const [selectOption, setSelectOption] = useState<BaseViewConfig>({
    list: [],
    component: "",
  });

  const selectComponent = (component: string) => {
    const option = config.components.find(
      (item) => item.component === component
    );

    if (option) {
      setSelectOption(option);
    } else {
      const obj: BaseViewConfig = {
        component: component,
        list: [],
      };
      setConfig({
        components: [...config.components, obj],
      });
      setSelectOption(obj);
    }
    setVisible(true);
  };

  const changeOption = (option: BaseViewConfig) => {
    setSelectOption(option);
    const newConfig = { ...config };
    let index = newConfig.components.findIndex(
      (item) => item.component === option.component
    );
    newConfig.components[index] = option;
    setConfig(newConfig);
  };

  const print = () => {
    console.log(config);
    console.log("mock data:", table);
  };

  const save = () => {
    window.localStorage.setItem("config", JSON.stringify(config));
  };

  const load = () => {
    const config = window.localStorage.getItem("config");
    if (config) {
      setConfig(JSON.parse(config));
    }
  };

  const clear = () => {
    setConfig({ components: [] });
  };

  const leftConfig = config.components.find(
    (item) => item.component === "left"
  );

  const rightConfig = config.components.find(
    (item) => item.component === "right"
  );

  useEffect(() => {
    load();
  }, []);

  return (
    <>
      <HeaderScreen
        print={() => print()}
        save={() => save()}
        load={() => load()}
        clear={() => clear()}
      />
      <Row className={styles.config}>
        <Col
          span={6}
          className={styles.left}
          onClick={() => selectComponent("left")}
        >
          {leftConfig ? <ShowScreen config={leftConfig} /> : null}
        </Col>
        <Col
          span={18}
          className={styles.right}
          onClick={() => selectComponent("right")}
        >
          {rightConfig ? <ShowScreen config={rightConfig} /> : null}
        </Col>
      </Row>
      <ConfigScreen
        option={selectOption}
        visible={visible}
        change={changeOption}
        close={() => {
          setVisible(false);
        }}
      />
    </>
  );
};
