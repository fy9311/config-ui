import { Button, Space } from "antd";
import { Header } from "antd/lib/layout/layout";
import styles from "../index.module.less";

interface Props {
  save: () => void;
  print: () => void;
  load: () => void;
  clear: () => void;
}

export const HeaderScreen: React.FC<Props> = ({ save, print, load, clear }) => {
  return (
    <Header className={styles.header}>
      <Space>
        <Button type={"primary"} onClick={() => print()}>
          打印
        </Button>
        <Button type={"primary"} onClick={() => save()}>
          保存
        </Button>
        <Button type={"primary"} onClick={() => load()}>
          加载
        </Button>
        <Button type={"primary"} onClick={() => clear()}>
          清除
        </Button>
      </Space>
    </Header>
  );
};
