import { Header } from "antd/lib/layout/layout";
import { BaseViewConfig } from "../../types/view";
import styles from "./index.module.less";
import { table } from "../../mock";
import { BaseConfig, RenderType } from "../../types/config";
import { Link } from "react-router-dom";
import * as qs from "qs";

interface Props {
  config: BaseViewConfig;
}

export const ShowScreen: React.FC<Props> = ({ config }) => {
  const { name, list } = config;

  const singleShow = (item: BaseConfig) => {
    const params = item.params?.map((item) => {
      if (item.type === "custom") {
        return `${item.key}=${item.value}`;
      } else {
        return `${item.key}=${table[item.column[0]]}`;
      }
    });
    const path = params ? `${item.path}?${params.join("&")}` : item.path;
    return (
      <div>
        <span style={{ marginRight: "20px" }}>{item.displayName}:</span>
        {item.navigate ? (
          <Link to={path || ""}>{table[item.key]}</Link>
        ) : (
          <span style={{ color: "green" }}>{table[item.key]}</span>
        )}
      </div>
    );
  };

  const multiShow = (item: BaseConfig) => {
    return (
      <div>
        <span>{item.displayName}:</span>
        <span>{table[item.key].join(",")}</span>
      </div>
    );
  };

  const listShow = (item: BaseConfig) => {
    return (
      <div>
        <div>{item.displayName}:</div>
        {table[item.key].map((v: any, index: number) => (
          <div key={index}>
            {item.showKeys!.map((key, index) => (
              <span key={index}>
                {key}:{v[key]},
              </span>
            ))}
          </div>
        ))}
      </div>
    );
  };

  return (
    <div>
      <Header className={styles.header}>{name || "未命名"}</Header>
      <ul>
        {list.map((item) => (
          <li key={item.key}>
            {item.renderType === RenderType.SINGLE && singleShow(item)}
            {item.renderType === RenderType.MULTI && multiShow(item)}
            {item.renderType === RenderType.LIST && listShow(item)}
          </li>
        ))}
      </ul>
    </div>
  );
};
