import { Button, Divider, Drawer, Input, Radio, Select } from "antd";
import React from "react";
import { BaseViewConfig } from "../../types/view";
import { table } from "../../mock";
import { BaseConfig, RenderType } from "../../types/config";
import { SetLink } from "./link";

const { Option } = Select;

interface Props {
  visible: boolean;
  close: () => void;
  option: BaseViewConfig;
  change: (option: BaseViewConfig) => void;
}

const renderTypeList = [
  {
    value: RenderType.SINGLE,
    label: "key:value",
  },
  {
    value: RenderType.MULTI,
    label: "key:value,value...",
  },
  {
    value: RenderType.LIST,
    label: "[key:value,key:value]",
  },
];

export const ConfigScreen: React.FC<Props> = ({
  visible,
  close,
  option,
  change,
}) => {
  const { list, name } = option as BaseViewConfig;
  const keys = Object.keys(table);

  const addShowCase = () => {
    const obj = { ...option };
    obj.list.push({
      key: "",
      displayName: "",
      renderType: "",
      navigate: false,
    });
    change(obj);
  };

  const deleteShowCase = (index: number) => {
    const obj = { ...option };
    obj.list.splice(index, 1);
    change(obj);
  };

  const chengKeyValue = (v: string, index: number, key: string) => {
    const obj = { ...option };
    obj.list[index][key] = v;
    change(obj);
  };
  const changeRenderType = (v: string, index: number) => {
    const obj = { ...option };
    if (v === RenderType.LIST) {
      if (!obj.list[index].sourceKeys) {
        obj.list[index].sourceKeys = Object.keys(table[obj.list[index].key][0]);
      }
      if (!obj.list[index].showKeys) {
        obj.list[index].showKeys = [];
      }
    }
    obj.list[index].renderType = v;
    change(obj);
  };
  const changeShowKeys = (v: string[], index: number) => {
    const obj = { ...option };
    obj.list[index].showKeys = v;
    change(obj);
  };
  const changeModuleName = (v: string) => {
    const obj = { ...option, name: v };
    change(obj);
  };
  const changeLinkStatus = (v: boolean, index: number) => {
    const obj = { ...option };
    obj.list[index].navigate = v;
    obj.list[index].path = obj.list[index].path || "";
    obj.list[index].params = obj.list[index].params || [];
    change(obj);
  };

  const setLink = (v: BaseConfig, index: number) => {
    const obj = { ...option };
    obj.list[index] = v;
    change(obj);
  };

  return (
    <Drawer
      title={name || "未命名"}
      placement="right"
      visible={visible}
      onClose={close}
    >
      <div>模块名:</div>
      <Input
        type="text"
        placeholder="设置当前模块标题"
        value={name}
        onChange={(e) => changeModuleName(e.target.value)}
      />
      <Divider />

      {list.map((item, index) => (
        <div key={index}>
          {/* 选择key */}
          <Select
            value={item.key || undefined}
            style={{ width: 200 }}
            placeholder="Select a key"
            onChange={(v) => chengKeyValue(v, index, "key")}
          >
            {keys.map((item) => (
              <Option key={item} value={item}>
                {item}
              </Option>
            ))}
          </Select>
          <Input
            type="text"
            placeholder="Set show name"
            value={item.displayName || undefined}
            onChange={(e) =>
              chengKeyValue(e.target.value, index, "displayName")
            }
          />
          <Select
            style={{ width: 200 }}
            placeholder="Select display type"
            onChange={(v: string) => changeRenderType(v, index)}
            value={item.renderType}
          >
            {renderTypeList.map((item) => (
              <Option key={item.value} value={item.value}>
                {item.label}
              </Option>
            ))}
          </Select>
          {item.renderType === RenderType.LIST && (
            <Select
              onChange={(v: string[]) => changeShowKeys(v, index)}
              value={item.showKeys}
              mode="multiple"
              placeholder="Select display key"
              style={{ width: 200 }}
            >
              {item.sourceKeys?.map((key) => (
                <Option key={key} value={key}>
                  {key}
                </Option>
              ))}
            </Select>
          )}
          <Radio.Group
            onChange={(e) => changeLinkStatus(e.target.value, index)}
            value={item.navigate}
            defaultValue={item.navigate || false}
          >
            <Radio value={true}>link</Radio>
            <Radio value={false}>text</Radio>
          </Radio.Group>
          {item.navigate && (
            <SetLink
              item={item}
              onChange={(v) => setLink(v, index)}
              keys={keys}
            />
          )}
          <Button type={"primary"} danger onClick={() => deleteShowCase(index)}>
            Delete
          </Button>
          <Divider />
        </div>
      ))}
      <Button type={"primary"} onClick={addShowCase}>
        添加展示项
      </Button>
    </Drawer>
  );
};
