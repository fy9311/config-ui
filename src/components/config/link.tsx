import { Button, Input, Radio, Select } from "antd";
import { useMemo } from "react";
import { BaseConfig, RenderType } from "../../types/config";
import { table } from "../../mock";

interface Props {
  onChange: (data: BaseConfig) => void;
  item: BaseConfig;
  keys: string[];
}

const { Option } = Select;

export const SetLink: React.FC<Props> = ({ onChange, item, keys }) => {
  const listConfig = () => {};

  const data = useMemo(() => {
    return { ...item };
  }, [item]);

  const addParams = () => {
    data.params?.push({
      type: "custom",
      key: "",
      value: "",
      column: [],
      subType: "",
      subKeys: [],
    });
    onChange(data);
  };

  const changePath = (v: string) => {
    data.path = v;
    onChange(data);
  };

  const deleteParams = (index: number) => {
    data.params?.splice(index, 1);
    onChange(data);
  };

  const changeParamType = (v: "custom" | "select", index: number) => {
    data.params![index].type = v;
    onChange(data);
  };

  const changeParamKey = (v: string, index: number) => {
    data.params![index].key = v;
    onChange(data);
  };

  const changeParamValue = (v: string, index: number) => {
    data.params![index].value = v;
    onChange(data);
  };

  const changeParamColumn = (v: string, index: number) => {
    data.params![index].column[0] = v;
    console.log();
    if (typeof table[v] === "object") {
      if (Array.isArray(table[v])) {
        data.params![index].subKeys = Object.keys(table[v][0]);
        data.params![index].subType = "array";
      } else {
        data.params![index].subKeys = Object.keys(table[v]);
        data.params![index].subType = "object";
      }
    } else {
      data.params![index].subType = "";
      data.params![index].subKeys = [];
    }
    onChange(data);
  };

  const changeParamSubColumn = (v: string, index: number) => {
    data.params![index].column[1] = v;
    onChange(data);
    console.log(data.params![index]);
  };

  const normalConfig = () => {
    const temp = item.params?.map((param, index) => (
      <div key={index}>
        <Radio.Group
          value={param.type}
          defaultValue={"custom"}
          onChange={(e) => changeParamType(e.target.value, index)}
        >
          <Radio value={"custom"}>custom</Radio>
          <Radio value={"select"}>select</Radio>
        </Radio.Group>
        <Input
          type="text"
          placeholder="Set query param key"
          value={param.key}
          onChange={(e) => changeParamKey(e.target.value, index)}
        />
        {param.type === "custom" ? (
          <Input
            type="text"
            placeholder="Set query param value"
            value={param.value}
            onChange={(e) => changeParamValue(e.target.value, index)}
          />
        ) : (
          <Select
            value={param.column[0]}
            placeholder="Select value key"
            style={{ width: 200 }}
            onChange={(v) => changeParamColumn(v, index)}
          >
            {keys.map((key) => (
              <Option key={key} value={key}>
                {key}
              </Option>
            ))}
          </Select>
        )}
        {param.subType && (
          <Select
            value={param.column[1]}
            placeholder="Select value key"
            style={{ width: 200 }}
            onChange={(v) => changeParamSubColumn(v, index)}
          >
            {param.subKeys.map((key) => (
              <Option key={key} value={key}>
                {key}
              </Option>
            ))}
          </Select>
        )}
        <Button
          style={{ marginLeft: "auto" }}
          type={"primary"}
          onClick={() => deleteParams(index)}
          danger
        >
          Delete param
        </Button>
      </div>
    ));

    return temp;
  };

  return (
    <div>
      <Input
        type="text"
        placeholder="Set link path(/detail)"
        value={item.path}
        onChange={(e) => changePath(e.target.value)}
      />
      {item.renderType === RenderType.LIST ? listConfig() : normalConfig()}
      <Button style={{ marginLeft: "auto" }} type={"link"} onClick={addParams}>
        Add param
      </Button>
    </div>
  );
};
